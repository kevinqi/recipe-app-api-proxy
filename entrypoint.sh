#!/bin/sh

# Make sure all commands succeed
set -e

# Do env var substitution and then write to default file path
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Don't run in daemon mode; docker apps should run in foreground and get logs
nginx -g 'daemon off;'
