# recipe-app-api-proxy

DevOps Deployment Automation Course - NGINX proxy
https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19173850#overview

NGINX proxy app for recipe app API.

https://gitlab.com/kevinqi/recipe-app-api-proxy

## Usage

Rebuild image locally:

```
docker build -t proxy .
```

### Environment Variables

* `LISTEN_PORT` - Port ot listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
